const grpc = require("@grpc/grpc-js");
const protoLoader = require("@grpc/proto-loader");
const echoProto = protoLoader.loadSync("echo.proto", {});
const echoDefinition = grpc.loadPackageDefinition(echoProto);
const {echoPackage} = echoDefinition;
const serverURL = "localhost:5000";
const server = new grpc.Server();
function EchoUnary(call, callback) {
    console.log('Call :', call.request);
    callback(null, {
        message : "received"
    })
}
function EchoClientStream(call, callback) {
    const list = []
    call.on("data", data => {
        list.push(data)
        console.log("Server: ", data);
    })
    call.on("end", err => {
        console.log("list : ",list);
        if(err) console.log("error :", err);
    })
}
function EchoServerStream(call, callback) {
    for (let index = 0; index < 10; index++) {
        call.write({
            value: index
        })
    }
    call.on("end", err => {
        console.log(err);
    })
}
function dateTime(call, callback) {
    call.on("data", data=> {
        console.log("Server DateTime: ", data);
        call.write({value : new Date().toLocaleString()})
    });
    call.on("end", err => {
        console.log(err);
    })
}
server.addService(echoPackage.EchoService.service, {
    EchoUnary,
    EchoClientStream,
    EchoServerStream,
    dateTime
});
server.bindAsync(serverURL, grpc.ServerCredentials.createInsecure(), callback => {
    server.start()
    console.log("run over localhost:5000");
});
